#!/usr/bin/env python
import webapp2
import os
import cgi
import datetime
import wsgiref.handlers
import time
from datetime import datetime

from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext.db import Model
from google.appengine.api import mail

from google.appengine.ext.webapp import template

eventtypes = {'run','basketball','soccer','golf'}
groups = {'runners','work players','saturday soccer group','my golf buddies'}

# Data
class Event(db.Model):
	who = db.StringProperty()
	what = db.StringProperty()
	where = db.StringProperty()
	when = db.StringProperty()
	why = db.StringProperty()
	name = db.StringProperty()
	participants = db.StringProperty()
	deadline = db.StringProperty()
	created = db.DateTimeProperty()
	def to_dict(self):
		return dict([(p, unicode(getattr(self, p))) for p in self.properties()])
	def is_valid(self):
		return len(self.name)!=0

class User(db.Model):
	name = db.StringProperty()
	password = db.StringProperty()
	created = db.DateTimeProperty()
	def to_dict(self):
		return dict([(p, unicode(getattr(self, p))) for p in self.properties()])

class UserEvent(db.Model):
	user = db.StringProperty()
	event = db.StringProperty()
	created = db.DateTimeProperty()
	def to_dict(self):
		return dict([(p, unicode(getattr(self, p))) for p in self.properties()])

#logic
class CreateHandler(webapp2.RequestHandler):
    def saveEvent(self):
                    event = Event()
                    event.who = self.request.get("who")
                    event.what = self.request.get("what")
                    event.where = self.request.get("where")
                    event.when = self.request.get("when")
                    event.why = self.request.get("why")
                    event.name = self.request.get("name")
                    event.participants = self.request.get("participants")
                    event.deadline = self.request.get("deadline")
					event.created = datetime.now()
                    if event.is_valid():
					    event.put()
						self.redirect("/find")
					else:
						self.redirect("/create")
        
    def post(self):
        self.saveEvent()
        query = "SELECT * FROM Event ORDER BY created DESC"
	events = db.GqlQuery(query)
        
        template_values = {
            'name': 'jon',
            'eventtypes': eventtypes,
            'groups': groups,
            'events': events}
        path = os.path.join(os.path.dirname(__file__), 'create.html')
        self.response.out.write(template.render(path, template_values))
        
    def get(self):
        template_values = {
            'name': 'jon',
            'eventtypes': eventtypes,
            'groups': groups,
            'url_linktext': 'next...'
        }
        path = os.path.join(os.path.dirname(__file__), 'create.html')
        self.response.out.write(template.render(path, template_values))

class FindHandler(webapp2.RequestHandler):
    def post(self):
        self.get()
    def get(self):
        url = 'sudp'
        
	query = "SELECT * FROM Event ORDER BY created DESC"
	events = db.GqlQuery(query)

        user = self.request.get("user")
        userkey = self.request.get("userkey")

        appquery = "SELECT * FROM UserEvent"
        appquery += " WHERE user = :1"
        appquery += " ORDER BY created DESC"
        appevents = db.GqlQuery(appquery, user)

        userevents =[]
        for ue in appevents:
                event = db.get (ue.event)
                userevents.append(event)

                    
        template_values = {
            'name': user,
            'eventtypes': eventtypes,
            'groups': groups,
            'events': events,
            'appevents': appevents,
            'userevents': userevents,
            'url_linktext': 'next...'
        }
        path = os.path.join(os.path.dirname(__file__), 'find.html')
        self.response.out.write(template.render(path, template_values))

class DetailsHandler(webapp2.RequestHandler):
    def post(self):
        self.get()
    def get(self):
        eventkey = self.request.get("eventkey")

        event = None
        if len(eventkey) != 0: 
                event = db.get (eventkey)

        user = self.request.get("user")

        userevents=[]
        if len(eventkey) != 0:
                userquery = "SELECT * FROM UserEvent"
                userquery += " WHERE event = :1"
                userquery += " ORDER BY created DESC"
                userevents = db.GqlQuery(userquery, eventkey)
                    
        template_values = {
            'user': user,
            'eventtypes': eventtypes,
            'groups': groups,
            'event': event,
            'userevents': userevents,
            'eventkey': eventkey,
            'url_linktext': 'next...'
        }
        path = os.path.join(os.path.dirname(__file__), 'details.html')
        self.response.out.write(template.render(path, template_values))

class LoginHandler(webapp2.RequestHandler):
    def post(self):
        name = self.request.get("name")
        password = self.request.get("password")
        if len(name) != 0:
                userquery = "SELECT * FROM User"
                userquery += " WHERE name = :1"
                user = db.GqlQuery(userquery, name)
                if !user
                    user = User()
                    user.name = name
                    user.password = password
                    user.created = datetime.now()
                    user.put()
        uri = "/find?user="+name
        self.redirect(uri)
    def get(self):
        template_values = {
            'name': 'jon',
            'url_linktext': 'next...'
        }
        path = os.path.join(os.path.dirname(__file__), 'login.html')
        self.response.out.write(template.render(path, template_values))

class JoinHandler(webapp2.RequestHandler):
    def post(self):
        self.doJoin()
        
    def get(self):
        self.doJoin()

    def doJoin(self):
        user = self.request.get("user")
        eventkey = self.request.get("eventkey")

        userquery = "SELECT * FROM UserEvent"
        userquery += " WHERE event = :1 AND user = :2"
        userquery += " ORDER BY created DESC"
        userevents = db.GqlQuery(userquery, eventkey, user)

        if userevents.count != 0:
                userevent = UserEvent()
                userevent.user = user
                userevent.event = eventkey
                userevent.created = datetime.now()
                userevent.put()

                message = mail.EmailMessage(sender="ludo <jseller27@gmail.com>",
                            subject="Your account has been approved")
                message.to = user
                message.body = """
                    Hi.

                    It's ludo calling, you in or out?

                    Please let us know if you have any questions.

                    The ludo Team
                    """
                message.send()
        uri = "/find?user="+user
        self.redirect(uri)
                
class AddPeopleHandler(webapp2.RequestHandler):
    def post(self):
        self.doAddPeople()
        
    def get(self):
        self.doAddPeople()

    def doAddPeople(self):
        user = self.request.get("user")
        eventkey = self.request.get("eventkey")
        users = self.request.get("users")

        userlist = users.strip().split(',')
        #print userlist

        event = db.get (eventkey)
        
        for auser in userlist:
                subject = "Yo, yo, wasssssup cuz? You have been invited to "+event.name
                body = "Hi "+auser
                body+= "It's ludo, you have been invited to "+event.name
                body+= "It's on "+event.when
                body+= " taking place at "+event.where
                body+= " Please let us know if you have any questions. Thanks, The ludo Team"
                message = mail.EmailMessage(sender="ludo <jseller27@gmail.com>",
                            subject="You have been invited to an event!")
                message.to = auser
                message.body = body
                message.send()
        uri = "/details?user="+user+"&eventkey="+eventkey
        self.redirect(uri)

        

app = webapp2.WSGIApplication([('/', LoginHandler),
			('/create', CreateHandler),
                        ('/addpeople', AddPeopleHandler),
                        ('/details', DetailsHandler),
                        ('/join', JoinHandler),
			('/find', FindHandler)],
                              debug=True)


